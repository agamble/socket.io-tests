var express = require('express')
  , app 	= express()
  , server 	= require('http').createServer(app)
  , io 		= require('socket.io').listen(server);


server.listen(8080);

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});

app.use(express.static(__dirname + "/public"));


var counter = 0;

var userarr = [];


io.sockets.on('connection', function (socket) {
	counter++;
	socket.broadcast.emit('counter', { "number": counter });
	socket.emit('counter', {"number": counter});
	socket.emit('userlist', userarr);

	socket.on('disconnect', function(socket) {
		counter--;
		console.log("disconnect");
		io.sockets.emit('counter', { "number": counter });
	});

	socket.on('username', function (data) {
		console.log("A new user '" + data + "'has connected.");
		userarr.push(data)
		socket.broadcast.emit('username', data);
		socket.emit('username', data)
	});
});



